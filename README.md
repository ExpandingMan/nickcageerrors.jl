# NickCageErrors

Use [Nicolas Cage](https://en.wikipedia.org/wiki/Nicolas_Cage) to appropriately express user
frustration when encountering errors.

![](/assets/nick_cage_errors_1.webm)


**NOTE**: `mpv_jll` needs to be updated, so for now this requires `mpv` to already be installed.
