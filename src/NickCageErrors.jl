module NickCageErrors

const DEFAULT_SOURCE = "https://youtu.be/4zySHepF04c"

const MPV_EXE = "mpv"


struct Clip
    title::String
    source::String
    start::Float64
    stop::Float64
end

function Clip(title::AbstractString, start::Real, stop::Real;
              source::AbstractString=DEFAULT_SOURCE,
             )
    Clip(title, source, start, stop)
end

duration(c::Clip) = c.stop - c.start

function playcmd(c::Clip)
    Cmd([MPV_EXE,
         "--no-config",
         "--title='$(c.title)'",
         "--no-border",
         "--cursor-autohide=always",
         "--really-quiet",
         "--load-stats-overlay=no",
         "--start="*string(c.start),
         "--end="*string(c.stop),
         c.source,
        ]
       )
end

play(c::Clip) = run(playcmd(c))


function _allclips()
    [Clip("moaning1", 9, 14),
     Clip("im_a_vampire", 23, 29),
     Clip("alphabet", 38, 49),
     Clip("fucking_files", 60 + 12.6, 60 + 19.4),
     Clip("fuck", 60 + 19.5, 60 + 25),
     Clip("i_hate_you_both", 60 + 38, 60 + 42),
     Clip("piss_blood", 60 + 43, 60 + 47),
     Clip("laughing1", 2*60 + 24, 2*60 + 35),
     Clip("punch", 2*60 + 38, 2*60 + 48),
     Clip("honey", 2*60 + 59, 3*60 + 5),
     Clip("hysterical1", 3*60 + 10, 3*60 + 19),
     Clip("not_the_bees", 3*60 + 24, 3*60 + 36),
    ]
end

allclips() = Dict(c.title=>c for c ∈ _allclips())


const CLIPS = allclips()


default_chooseclip() = quote
    __chooseclip__(e::Exception) = rand(collect(values(CLIPS)))
end

replace_error_expr() = quote
    function Base.showerror(io::IO, e, bt; backtrace=true)
        try
            c = __chooseclip__(e)
            println(io, "fuck. ", c.title)
            showerror(io, e)
            backtrace && Base.show_backtrace(io, bt)
            play(c)
        catch e′
            @error("even Nick Cage can't handle this error")
            rethrow(e′)
        end
    end
end

function nickcage!(expr=default_chooseclip())
    eval(expr)
    eval(replace_error_expr())
end


export nickcage!


end
